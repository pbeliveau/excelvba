Sub changeBold()
'change selection to bold if not bold, vice versa
' add shortcut for CTRL+b

Dim cel As Range
For Each cel In Selection
    If cel.Font.Bold = False Then
        cel.Font.Bold = True
    Else
        cel.Font.Bold = False
    End If
Next cel
End Sub