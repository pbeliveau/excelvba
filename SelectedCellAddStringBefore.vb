Sub addStringBefore()
' add value before the one in cell
' add shortcut for CTRL+SHIFT+B
' only works one cell at a time

Dim cellString As String, newString As String, newCellString As String

cellString = Selection.Value
newString = InputBox("Entrer ce que vous voulez ajouter avant :")
newCellString = newString & cellString
Selection.Value = newCellString
End Sub