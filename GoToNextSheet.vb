Sub ctrlNext()
' change sheet to the next one
' add shortcut for CTRL+N
' based on emacs movement

Dim sheetIndex As Integer, lastSheet As Integer
On Error GoTo ErrHandler
sheetIndex = ActiveWorkbook.ActiveSheet.Index + 1
lastSheet = ActiveWorkbook.Sheets.Count

Do While Worksheets(sheetIndex).Visible = xlSheetHidden
    sheetIndex = sheetIndex + 1
Loop

ActiveWorkbook.Worksheets(sheetIndex).Activate

ErrHandler:
    Select Case Err.Number
        Case 9
            If sheetIndex > 0 And sheetIndex < lastSheet Then
                sheetIndex = sheetIndex - 1
                ActiveWorkbook.Worksheets(sheetIndex).Activate
                Resume
            ElseIf sheetIndex >= lastSheet Then
                MsgBox "You're at the last sheet."
                Exit Sub
            End If
    End Select
    
End Sub