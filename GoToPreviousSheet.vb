Sub ctrlPrevious()
' change sheet to the previous one
' add shortcut for CTRL+P
' based on emacs movement

Dim sheetIndex As Integer
On Error GoTo ErrHandler
sheetIndex = ActiveWorkbook.ActiveSheet.Index - 1

Do While Worksheets(sheetIndex).Visible = xlSheetHidden
    sheetIndex = sheetIndex - 1
Loop

ActiveWorkbook.Worksheets(sheetIndex).Activate

ErrHandler:
    Select Case Err.Number
        Case 9
            If sheetIndex > 0 Then
                sheetIndex = sheetIndex - 1
                ActiveWorkbook.Worksheets(sheetIndex).Activate
                Resume
            ElseIf sheetIndex = 0 Then
                MsgBox "You're at the first sheet."
                Exit Sub
            End If
    End Select
    
End Sub