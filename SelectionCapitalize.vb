Sub capitalLetters()
' change letters in selection to all upercase
' add shortcut for CTRL+u

Dim accName As String, accNameUcase As String, cel As Range
For Each cel In Selection
    If cel.Value <> "" Then
        accName = cel.Value
        accNameUcase = UCase(accName)
        cel.Value = accNameUcase
    End If
Next cel
End Sub