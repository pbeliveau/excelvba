Sub editString()
' edit the whole string in a input box
' only works one cell at a time

Dim cellString As String, newString As String, newCellString As String

cellString = Selection.Value
newString = InputBox("Entrer ce que vous voulez ajouter apres :", "Change value" _
                        , cellString)
Selection.Value = newString

End Sub