Sub addTitleLine()
' add line to selection sligthly gray
' add shortcut for CTRL+l

Dim cel As Range
For Each cel In Selection
    With cel.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ThemeColor = 2
        .TintAndShade = 0.349986266670736
        .Weight = xlThin
    End With
Next cel
End Sub