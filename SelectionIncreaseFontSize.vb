Sub upSizeFont1point()
' up font size of selection by one
' add shortcut for CTRL+SHIFT+U

Dim actualSize As Integer, newSize As Integer, cel As Range
For Each cel In Selection
    actualSize = cel.Font.Size
    newSize = actualSize + 1
    cel.Font.Size = newSize
Next cel
End Sub