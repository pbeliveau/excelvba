Sub Go2Middle()
' add shortcut for CTRL+m

Dim lastRow As Integer, activeColumn As Integer, midRow As Integer
activeColumn = ActiveCell.Column
lastRow = ActiveSheet.Cells(5000, activeColumn).End(xlUp).Row
midRow = lastRow / 2

Application.Goto ActiveSheet.Cells(midRow, activeColumn)
End Sub