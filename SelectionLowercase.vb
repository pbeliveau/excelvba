Sub lowerLetters()
' change letters in selection to all upercase
' add shortcut for CTRL+r

Dim accName As String, accNameLcase As String, cel As Range
For Each cel In Selection
    If cel.Value <> "" Then
        accName = cel.Value
        accNameLcase = LCase(accName)
        cel.Value = accNameLcase
    End If
Next cel