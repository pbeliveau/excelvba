Sub changeItalic()
'change selection to italic if not italic, vice versa
' add shortcut for CTRL+i

Dim cel As Range
For Each cel In Selection
    If cel.Font.Italic = False Then
        cel.Font.Italic = True
    Else
        cel.Font.Italic = False
    End If
Next cel
End Sub