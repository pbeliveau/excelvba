Sub properLetters()
' capitalize only the first letter of strings
' add shortcut at CTRL+p

Dim cel As Range, accName As String
For Each cel In Selection
    accName = StrConv(cel.Value, vbProperCase)
    cel.Value = accName
Next cel
End Sub