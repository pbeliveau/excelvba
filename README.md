# ExcelVBA
Series of simple algorithms for Excel (VBA) to perform quick formating of cells with keyboard shortcuts.

## How to use 

1. Select the algorithm that you want and copy the raw contents
2. Open any Excel workbook and the Visual Basic Editor inside
3. Add a new module and paste the contents
4. In the "Developper" tab of Excel, select the macro name and add a keyboard shortcut to it
5. Enjoy!

## .vb extension
The .vb extension is use to help GitHub correctly identify this repo as using
VBA. Just copy-paste the contents of the files into one module in Excel.
