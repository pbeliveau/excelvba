Sub addStringAfter()
' add value after the one in cell
' add shortcut for CTRL+SHIFT+A
' only works one cell at a time

Dim cellString As String, newString As String, newCellString As String

cellString = Selection.Value
newString = InputBox("Entrer ce que vous voulez ajouter apres :")
newCellString = cellString & newString
Selection.Value = newCellString
End Sub